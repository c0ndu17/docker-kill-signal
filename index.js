const Server = require('hapi').Server
const ON_DEATH = require('death')
console.log('Running');

process.on('beforeExit', (code) => {
   console.log(`Caught BeforeExit code: ${code}`) 
})

process.on('exit', (code) => {
   console.log(`Caught Exit code: ${code}`) 
})

ON_DEATH((signal, err) => {
  console.log('KILLING');
  console.log(`SIGNAL: ${signal}`);
  process.exit()
})

const server = new Server({ 
})

server.options= {
  port: 3000
}

server.start(() => {
  console.log('STARTED')
})
